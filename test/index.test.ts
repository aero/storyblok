import {expect, test} from '@oclif/test'

import cmd = require('../src')

describe('storyblok', () => {
  test
  .do(() => cmd.run([]))
  .exit(2)
  .it('requires a file argument')

  test
  .stdout()
  .do(() => cmd.run(['invalid']))
  .it('requires a file', ctx => {
    expect(ctx.stdout).to.have.string('Error reading file')
  })

  test
  .stdout()
  .do(() => cmd.run(['test/bad.json']))
  .it('requires a JSON file', ctx => {
    expect(ctx.stdout).to.have.string('File needs to be JSON')
  })

  test
  .stdout()
  .do(() => cmd.run(['test/example.json']))
  .it('renders JSON', ctx => {
    expect(ctx.stdout).to.contain('<a href="https://aero.com/na" target="_blank" linktype="url">')
  })

  test
  .stdout()
  .do(() => cmd.run(['test/example.json', '-o', '/invalid/path']))
  .it('shows a warning if output path is invalid', ctx => {
    expect(ctx.stdout).to.have.string('Error writing file')
  })

  test
  .stdout()
  .do(() => cmd.run(['test/example.json', '-o', '/dev/null']))
  .it('saves the output to a file', ctx => {
    expect(ctx.stdout).to.have.string('Output saved to:')
  })
})
