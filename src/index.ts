import {Command, flags} from '@oclif/command'
import * as fs from 'fs'
const debug = require('debug')('storyblok')
import RichTextResolver = require('storyblok-js-client/dist/rich-text-resolver.cjs');

class Storyblok extends Command {
  static description = 'Storyblok CLI to transform RichText'

  static flags = {
    version: flags.version({char: 'v'}),
    help: flags.help({char: 'h'}),
    output: flags.string({char: 'o', description: 'Output file to save rendered'}),
  }

  static args = [{name: 'file', required: true, description: 'File to be rendered'}]

  async run() {
    const {args, flags} = this.parse(Storyblok)

    try {
      const jsonString = fs.readFileSync(args.file, 'utf8')

      try {
        const json = JSON.parse(jsonString)
        const resolver = new RichTextResolver()

        const html = resolver.render(json)
        if (flags.output) {
          try {
            fs.writeFileSync(flags.output, html)
            this.log('Output saved to: ', flags.output)
          } catch (error) {
            this.log('Error writing file. Please check if the path is valid and you have the right permissions.')
            debug(error)
          }
        } else {
          this.log(html)
        }
      } catch (error) {
        this.log('File needs to be JSON')
        debug(error)
      }
    } catch (error) {
      this.log('Error reading file. Please check if the path is correct and you have the right permissions.')
      debug(error)
    }
  }
}

export = Storyblok
