declare module 'storyblok-js-client/dist/rich-text-resolver.cjs' {
  interface Richtext {
    content: Array<Record<string, any>>;
  }

  interface RichtextInstance {
    render: (data: Richtext) => string;
  }

  class RichTextResolver {
    constructor()

    render: (data: Richtext) => string
  }

  export = RichTextResolver
}
